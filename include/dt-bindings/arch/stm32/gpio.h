#ifndef __DT_STM32_GPIO__
#define __DT_STM32_GPIO__

/* Mapping of DT macros to architecture dependant macros */

#define DT_GPIO_FLAGS(x) \
  ((x & DT_GPIO_INPUT        ? GPIO_INPUT           : 0) | \
   (x & DT_GPIO_OUTPUT       ? GPIO_OUTPUT          : 0) | \
   (x & DT_GPIO_PULLUP       ? GPIO_PULLUP          : 0) | \
   (x & DT_GPIO_PULLDOWN     ? GPIO_PULLDOWN        : 0) | \
   (x & DT_GPIO_PUSHPULL     ? GPIO_PUSHPULL        : 0) | \
   (x & DT_GPIO_OPENDRAIN    ? GPIO_OPENDRAIN       : 0) | \
   (x & DT_GPIO_FLOAT        ? GPIO_FLOAT           : 0) | \
   (x & DT_GPIO_EXTI         ? GPIO_EXTI            : 0) | \
   (x & DT_GPIO_OUTPUT_SET   ? DT_GPIO_OUTPUT_SET   : 0) | \
   (x & DT_GPIO_OUTPUT_CLEAR ? DT_GPIO_OUTPUT_CLEAR : 0))

#endif
