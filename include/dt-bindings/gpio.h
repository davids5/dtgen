#ifndef __GPIO_H__
#define __GPIO_H__

/* This should be defined based on architecture GPIO_* definitions */

#define DT_GPIO_PULLUP        (1 << 0)
#define DT_GPIO_PULLDOWN      (1 << 1)
#define DT_GPIO_PUSHPULL      (1 << 2)
#define DT_GPIO_OPENDRAIN     (1 << 3)
#define DT_GPIO_INPUT         (1 << 4)
#define DT_GPIO_OUTPUT        (1 << 5)
#define DT_GPIO_FLOAT         (1 << 6)
#define DT_GPIO_EXTI          (1 << 7)
#define DT_GPIO_OUTPUT_SET    (1 << 8)
#define DT_GPIO_OUTPUT_CLEAR  (1 << 9)

#endif
