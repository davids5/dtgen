# Device Tree on NuttX (testing ground)

The idea is to build up a mechanism for using devicetrees in NuttX in various ways. Device tree main source files (.dts), which can include other source files (.dtsi) are compiled into binary form (.dtb) using dtc compiler. 

From the final merged binary tree, a generator (`dtgen`) application reads all device description and proceeds to generate a header file (`devicetree_gen.h`) based on a set of binding files (defined in YAML format). A main header `devicetree.h` with manually written macro definitions includes the generated header. This main header is to be included in NuttX files to obtain information to configure devices.

A mockup application which emulates NuttX initialization is provided to demonstrate how devices are initialized.

# Build dtgen

For now I'm using cmake for simplicity. To build, do the following:

    mkdir build
    cd build
    cmake ..
    make

# Test

The mockup application is inside the `demo` dir. If you run `make` it will compile the DTS into a DTB, run the generator and then build the mockup which includes the generated header.

    ./mockup

# TODO

This is a work in progress. I'm slowly adding bindings to new driver types which motivates extending the generator app. There's quite a bit of work to do. Some considerations:

 - SPI: generate table for handling CS lines
 - Timers: register timers declared as PWMs, QEs
 - autoleds: allow assigning LED_* definitions (IDLE, PANIC, etc) from DT by using "chosen" key in DT.
 - ADCs: register each devices with its channels

More general aspects:

 - I'm using .dts -> .dtb -> .h pipeline. The .dtb parsing api is quite good but a possible alternative would be to use .dts -> .yaml -> .h since the dtc compiler supports generating a YAML file instead of the binary. 


