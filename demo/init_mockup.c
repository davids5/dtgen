#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <devicetree.h>
#include <dt-bindings/arch/stm32/gpio.h>

/****************************************************************************
 * Mockups
 ****************************************************************************/

#define GPIO_INPUT 0
#define GPIO_OUTPUT 0
#define GPIO_FLOAT 0
#define GPIO_PULLUP 0
#define GPIO_PULLDOWN 0
#define GPIO_PUSHPULL 0
#define GPIO_OPENDRAIN 0
#define GPIO_EXTI 0
#define GPIO_OUTPUT_SET 0
#define GPIO_OUTPUT_CLEAR 0

struct i2c_master_s {} g_i2c1;

struct i2c_master_s* stm32_i2cinitialize(int i2c)
{
  printf("Initialize I2C%i\n", i2c);
  return &g_i2c1;
}

int stm32_bmp280_initialize(const char* path, struct i2c_master_s* i2c)
{
  printf("Initialize bmp280 driver at %s\n", path);
  return 0;
}

/* This mimics the main appinit section */

int stm32_bringup(void)
{
  /* Initialize I2C buses */

  INIT_I2C;

  /* Initialize I2C sensors */

  INIT_I2C_SENSORS;

  return 0;
}

/* This mimics the buttons support section */
#if 0
void board_autoled_initialize(void)
{
  INIT_LEDS();
}
#endif

/* This mimics the autoleds section */

typedef int xcpt_t;
void stm32_configgpio(uint32_t cfg) { }
int stm32_gpioread(uint32_t cfg) { return 0; }
int stm32_gpiosetevent(uint32_t cfg, bool a, bool b, bool c, xcpt_t x, void* p) { return 0; }

#define GPIO_PORTA 0
#define GPIO_PIN0  0

uint32_t g_button_gpios[] =
{
  BUTTON_GPIO_CONFIGS
};

uint32_t board_button_initialize(void)
{
  int i;
  const int num_buttons = (sizeof(g_button_gpios) / sizeof(uint32_t));

  for (i = 0; i < num_buttons; i++)
  {
    stm32_configgpio(g_button_gpios[i]);
  }

  return num_buttons;
}

uint32_t board_buttons(void)
{
  const int num_buttons = (sizeof(g_button_gpios) / sizeof(uint32_t));
  uint32_t ret = 0;
  int i;

  for (i = 0; i < num_buttons; i++)
  {
    bool pressed = stm32_gpioread(g_button_gpios[i]);
    if (pressed) ret |= (1 << i);
  }

  return ret;
}

int board_button_irq(int id, xcpt_t irqhandler, void *arg)
{
  int ret = -EINVAL;

  /* The following should be atomic */

  ret = stm32_gpiosetevent(g_button_gpios[id], true, true, true,
                           irqhandler, arg);

  return ret;
}


int main(int argc, char** argv)
{
  stm32_bringup();
}
