#ifndef __DTGEN_FDT_H__
#define __DTGEN_FDT_H__

#include <istream>
#include <list>
#include <vector>

namespace dtgen
{
  class FDT
  {
    public:
      class Node
      {
        public:
          Node(uint8_t* fdt, int offset);

          operator bool() const;

          std::string name(void) const;

          bool has_property(const std::string& name) const;
          template<class T> T property(const std::string& name) const;

          Node path(const std::string& p) const;
          Node parent(void) const;
          Node child(void) const;
          Node next(void) const;
          Node from_handle(uint32_t handle) const;

        private:
          uint8_t* fdt = nullptr;
          int offset = -1;
      };

      FDT(void) = delete;
      FDT(std::istream& istream);

      FDT::Node get_path(const std::string& path) const;

    private:
      std::vector<uint8_t> buf;
      uint8_t* fdt = nullptr;
  };
}

#endif // __DTGEN_FDT_H__
