#ifndef __DTGEN_BINDING_H__
#define __DTGEN_BINDING_H__

#include <yaml-cpp/yaml.h>
#include <string>
#include <list>
#include "fdt.h"

namespace dtgen
{
  struct BindingOutput
  {
    std::map<std::string, std::list<std::string>> macro_depends;
    std::map<std::string, std::list<std::string>> macros;
    std::set<std::string> includes;
  };

  class Binding
  {
    public:
      Binding(void) = delete;
      Binding(YAML::Node yaml);

      bool matches_children(void);
      bool matches(const FDT::Node& node);

      void generate(const FDT::Node& node, BindingOutput& output);

    private:
      YAML::Node yaml;

      void generate_includes(const FDT::Node& node, BindingOutput& output);
      void generate_macros(const FDT::Node& node, BindingOutput& output);

      void apply_substitutions(const FDT::Node& node, std::string& s);
      std::string eval_expression(const FDT::Node& node,
                                  std::string expression);
  };
}

#endif // __DTGEN_BINDING_H__
