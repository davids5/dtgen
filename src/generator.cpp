#include <regex>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include "generator.h"

using namespace std::string_literals;

dtgen::Generator::Generator(const FDT& _fdt, std::ostream& _ostream,
                            const std::string& bindings_path)
  : fdt(_fdt), ostream(_ostream)
{
  load_bindings(bindings_path);
}

void dtgen::Generator::generate(void)
{
  parse(fdt.get_path("/"));
  define_macros();
}

void dtgen::Generator::load_bindings(const std::string& path)
{
  DIR* dir = opendir(path.c_str());

  if (!dir)
  {
    throw std::runtime_error("Could not open bindings path");
  }

  struct dirent* entry;
  while ((entry = readdir(dir)))
  {
    if (entry->d_name[0] == '.')
    {
      continue;
    }

    if (entry->d_type == DT_REG)
    {
      std::string filename(entry->d_name);
      if (filename.substr(filename.find('.') + 1) == "yaml")
      {
        filename = path + "/" + filename;
        std::ifstream file(filename);
        if (!file)
        {
          throw std::runtime_error(std::string("Failed to open ") + filename);
        }
        else
        {
          std::cout << "Parsing binding: " << filename << std::endl;
        }

        bindings.parse(file);
      }
    }
  }

  closedir(dir);
}

void dtgen::Generator::parse(const dtgen::FDT::Node& node)
{
  //std::cout << "Parsing children of " << node.name() << std::endl;

  /* parse nodes below this level in BFS order */

  std::list<FDT::Node> enabled_children;
  for (FDT::Node child_node = node.child(); child_node; child_node = child_node.next())
  {
    if (child_node.name() == "chosen" || child_node.name() == "aliases")
    {
      continue;
    }

    //std::cout << "Parsing node: " << child_node.name() << std::endl;

    if ((!child_node.has_property("status") || child_node.property<std::string>("status") == "okay") &&
        child_node.has_property("compatible"))
    {
      if (!bindings.process(child_node, binding_output))
      {
        /* did not match, try to parse children */

        enabled_children.push_back(child_node);
      }
    }
  }

  /* recurse into children */

  for (FDT::Node child_node = node.child(); child_node; child_node = child_node.next())
  {
    parse(child_node);
  }
}

void dtgen::Generator::define_macros(void)
{
  /* add all includes */

  for (const std::string& path : binding_output.includes)
  {
    ostream << "#include <" << path << ">" << std::endl;
  }

  ostream << std::endl;

  /* define all macros */

  for (const auto& [name, entries] : binding_output.macros)
  {
    std::string value;
    size_t entry_id = 0;
    for (const auto& entry : entries)
    {
      value += "\t" + entry + (entry_id != entries.size() - 1 ? " \\\n" : "");

      entry_id++;
    }

    auto it = binding_output.macro_depends.find(name);
    bool has_depends = (it != binding_output.macro_depends.end());

    if (has_depends)
    {
      std::string all_depends = "(";
      size_t entry_id = 0;
      for (std::string depends : it->second)
      {
        all_depends += depends + (entry_id != it->second.size() - 1 ? " && " : ")");
        entry_id++;
      }
      ostream << "#if " << all_depends << std::endl;
    }

    ostream << "#" << (has_depends ? "  " : "") << "define "
            << name << " \\" << std::endl << value << std::endl;

    if (has_depends)
    {
      ostream << "#else" << std::endl << "#  define " << name << std::endl
              << "#endif" << std::endl;
    }

    ostream << std::endl;
  }
}
