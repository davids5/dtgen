#ifndef __DTGEN_GENERATOR_H__
#define __DTGEN_GENERATOR_H__

#include <ostream>
#include <map>
#include <list>

#include "fdt.h"
#include "bindings.h"

namespace dtgen
{
  class Generator
  {
    public:
      Generator(void) = delete;
      Generator(const FDT& fdt, std::ostream& ostream,
                const std::string& bindings_path);

      void generate(void);
      void load_bindings(const std::string& path);

    private:
      const FDT& fdt;
      std::ostream& ostream;
      Bindings bindings;

      BindingOutput binding_output;

      void parse(const FDT::Node& node);
      void define_macros(void);

      void join_output(BindingOutput& local_output);
  };
}

#endif // __DTGEN_GENERATOR_H__
