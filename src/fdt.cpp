#include <vector>
#include <fstream>
#include <stdexcept>
#include <iostream>

#include "fdt.h"

extern "C"
{
  #include "libfdt.h"
}

dtgen::FDT::FDT(std::istream& istream)
  : buf((std::istreambuf_iterator<char>(istream)),
        std::istreambuf_iterator<char>())
{
  fdt = buf.data();

  if (buf.empty())
  {
    throw std::runtime_error("Failed to load DTB");
  }

  /* Sanity check */

  int ret = fdt_check_header(fdt);

  if (ret < 0)
  {
    throw std::runtime_error(fdt_strerror(ret));;
  }
}

dtgen::FDT::Node dtgen::FDT::get_path(const std::string& path) const
{
  int offset = fdt_path_offset(fdt, path.c_str());

  if (offset < 0)
  {
    throw std::runtime_error(std::string("path_offset: ") + fdt_strerror(offset));
  }

  return Node(fdt, offset);
}

dtgen::FDT::Node::Node(uint8_t* _fdt, int _offset)
  : fdt(_fdt), offset(_offset)
{

}

std::string dtgen::FDT::Node::name(void) const
{
  return fdt_get_name(fdt, offset, nullptr);
}

dtgen::FDT::Node dtgen::FDT::Node::child(void) const
{
  return Node(fdt, fdt_first_subnode(fdt, offset));
}

dtgen::FDT::Node::operator bool(void) const
{
  return offset >= 0;
}

dtgen::FDT::Node dtgen::FDT::Node::next(void) const
{
  return Node(fdt, fdt_next_subnode(fdt, offset));
}

dtgen::FDT::Node dtgen::FDT::Node::from_handle(uint32_t handle) const
{
  return Node(fdt, fdt_node_offset_by_phandle(fdt, handle));
}


bool dtgen::FDT::Node::has_property(const std::string& name) const
{
  return fdt_getprop(fdt, offset, name.c_str(), nullptr);
}

dtgen::FDT::Node dtgen::FDT::Node::path(const std::string& p) const
{
  return Node(fdt, fdt_path_offset(fdt, p.c_str()));
}

dtgen::FDT::Node dtgen::FDT::Node::parent() const
{
  return Node(fdt, fdt_parent_offset(fdt, offset));
}

template<> std::list<std::string> dtgen::FDT::Node::property(const std::string& name) const
{
  std::list<std::string> l;
  size_t list_size = fdt_stringlist_count(fdt, offset, name.c_str());
  for (size_t i = 0; i < list_size; i++)
  {
    l.emplace_back(fdt_stringlist_get(fdt, offset, name.c_str(), i, nullptr));
  }

  return l;
}

template<> std::string dtgen::FDT::Node::property(const std::string& name) const
{
  const char* ptr = (const char*)fdt_getprop(fdt, offset, name.c_str(), nullptr);

  if (!ptr)
  {
    throw std::runtime_error(std::string("No property named '") + name + ("' was found"));
  }

  return std::string((const char*)fdt_getprop(fdt, offset, name.c_str(), nullptr));
}

template<> uint32_t dtgen::FDT::Node::property(const std::string& name) const
{
  uint32_t* ptr = (uint32_t*)fdt_getprop(fdt, offset, name.c_str(), nullptr);

  if (!ptr)
  {
    throw std::runtime_error(std::string("No property named '") + name + ("' was found"));
  }

  return fdt32_ld(ptr);
}

template<> std::vector<uint32_t> dtgen::FDT::Node::property(const std::string& name) const
{
  int len;
  uint32_t* ptr = (uint32_t*)fdt_getprop(fdt, offset, name.c_str(), &len);

  if (!ptr)
  {
    throw std::runtime_error(std::string("No property named '") + name + ("' was found"));
  }

  std::vector<uint32_t> result;
  result.resize(len);

  for (int i = 0; i < len; i++)
  {
    result[i] = fdt32_ld(ptr + i);
  }

  return result;
}
