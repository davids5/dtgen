#include <iostream>
#include <regex>
#include "binding.h"

using namespace std::string_literals;

dtgen::Binding::Binding(YAML::Node _yaml)
  : yaml(_yaml)
{

}

bool dtgen::Binding::matches_children(void)
{
  return (yaml["match_children"] && yaml["match_children"].as<bool>());
}

bool dtgen::Binding::matches(const dtgen::FDT::Node& node)
{
  /* TODO: check conditions to further restrict match */

  return true;
}

void dtgen::Binding::generate(const dtgen::FDT::Node& node,
                              BindingOutput& output)
{
  generate_includes(node, output);
  generate_macros(node, output);
}

void dtgen::Binding::generate_includes(const dtgen::FDT::Node& node, dtgen::BindingOutput& output)
{
  YAML::Node includes_node = yaml["include"];

  if (!includes_node)
  {
    return;
  }

  if (includes_node.IsSequence())
  {
    for (const YAML::Node& include_entry : includes_node)
    {
      output.includes.insert(include_entry.as<std::string>());
    }
  }
  else
  {
    output.includes.insert(includes_node.as<std::string>());
  }
}

void dtgen::Binding::generate_macros(const dtgen::FDT::Node& node, dtgen::BindingOutput& output)
{
  YAML::Node macros_node = yaml["macros"];

  for (YAML::Node macro_node : macros_node)
  {
    std::string name = macro_node["name"].as<std::string>();
    std::string value;

    YAML::Node definition = macro_node["definition"];
    if (macro_node["type"].as<std::string>() == "function")
    {
      /* function name */
      std::string function_name = definition["name"].as<std::string>();

      /* function arguments */
      std::string args;
      YAML::Node args_node = definition["args"];
      for (size_t i = 0; i < args_node.size(); i++)
      {
        YAML::Node arg_node = args_node[i];
        args += arg_node.as<std::string>() + (i != args_node.size() - 1 ? ", " : "");
      }

      /* return */
      std::string return_str, return_check_str;
      YAML::Node returns = definition["returns"];
      if (returns)
      {
        std::string return_type = returns["type"].as<std::string>();
        std::string return_name = returns["name"].as<std::string>();

        return_str = return_type + " " + return_name + " = ";

        YAML::Node return_check = returns["check"];
        if (return_check)
        {
          if (return_check.as<std::string>() == "pointer")
          {
            return_check_str = "if (!"s + return_name + ") {" +
                               "fprintf(stderr, \"ERROR: " +
                               function_name + " failed\"); }";
          }
        }
      }

      value += return_str + function_name + "(" + args + ");";

      if (!return_check_str.empty())
      {
        value += " \\\n\t" + return_check_str;
      }
    }
    else if (macro_node["type"].as<std::string>() == "raw")
    {
      value = definition.as<std::string>();
    }

    apply_substitutions(node, value);

    /* append resulting macros */

    std::list<std::string>& entries = output.macros[name];
    bool add_once = (macro_node["once"] && macro_node["once"].as<bool>());

    if (!add_once || (add_once && std::find(entries.begin(), entries.end(), value) == entries.end()))
    {
      entries.push_back(value);
    }

    /* process "depends" */

    if (macro_node["depends"])
    {
      std::string condition = macro_node["depends"].as<std::string>();
      std::list<std::string>& depend_entries = output.macro_depends[name];
      if (std::find(depend_entries.begin(), depend_entries.end(), condition) ==
          depend_entries.end())
      {
        depend_entries.push_back(condition);
      }
    }
  }
}

void dtgen::Binding::apply_substitutions(const dtgen::FDT::Node& node, std::string& s)
{
  /* match <...> tags */

  std::string output;
  std::regex tag_regex("(<[^>]+>)");
  std::smatch match;

  while (std::regex_search(s, match, tag_regex))
  {
    std::string tag = match.str();
    std::cout << "Found tag: " << tag << std::endl;

    /* save everything before the match */

    output += s.substr(0, match.position());

    /* perform the replacement */

    std::string prefix = tag.substr(0, 2);
    if (prefix == "<." || prefix == "</")
    {
      output += eval_expression(node, tag.substr(1, tag.size() - 2));
    }
    else if (tag == "<instance_number>")
    {
      if (node.has_property("label"))
      {
        std::string label = node.property<std::string>("label");
        std::string instance_number = label.substr(label.find('_') + 1);
        output += instance_number;
      }
    }
    else if (tag == "<parent_instance_number>")
    {
      if (node.parent().has_property("label"))
      {
        std::string label = node.parent().property<std::string>("label");
        std::string instance_number = label.substr(label.find('_') + 1);
        output += instance_number;
      }
    }
    else if (tag == "<soc_family>")
    {
      std::string soc_family = node.path("/soc").property<std::string>("family");
      output += soc_family;
    }
    else if (tag == "<address>")
    {
      if (node.has_property("reg"))
      {
        std::string address = std::to_string(node.property<uint32_t>("reg"));
        output += address;
      }
      else
      {
        throw std::runtime_error("no reg property found");
      }
    }
    else
    {
      throw std::runtime_error("unknown tag "s + tag);
    }

    /* consume everything up to and including the match */

    s = s.substr(match.position() + match.length());
  }

  /* add trailing string */

  s = output + s;
}

std::string dtgen::Binding::eval_expression(const dtgen::FDT::Node& node,
                                            std::string expression)
{

  /* refers to local property */
  if (expression[0] == '.')
  {
    std::string property_name = expression.substr(1, expression.find("[") - 1);
    std::cout << "property expression: " << property_name << std::endl;

    if (node.has_property(property_name))
    {
      std::string property_element = expression.substr(property_name.size() + 1, 3);
      if (!property_element.empty())
      {
        std::cout << "element: " << property_element << std::endl;
        std::vector<uint32_t> cell_array = node.property<std::vector<uint32_t>>(property_name);
        size_t element_idx = std::stoi(property_element.substr(1, 1));
        uint32_t val = cell_array[element_idx];

        std::cout << "char: " << expression.substr(expression.find("]") + 1, 1) << std::endl;
        if (expression.substr(expression.find("]") + 1, 1) == ".")
        {
          FDT::Node referenced_node = node.from_handle(val);
          std::string subproperty_name = expression.substr(expression.find("]") + 2);
          std::cout << "subproperty: " << subproperty_name << std::endl;
          if (referenced_node && referenced_node.has_property(subproperty_name))
          {
            return referenced_node.property<std::string>(subproperty_name);
          }
        }
        else
        {
          return std::to_string(val);
        }
      }
    }
  }
  /* refers to absolute path */
  else if (expression[0] == '/')
  {

  }

  return std::string();
}
