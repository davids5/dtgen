#include <iostream>
#include <vector>
#include <fstream>
#include "generator.h"
#include "fdt.h"

int main(int argc, char** argv)
{
  /* Print help */

  if (argc < 4)
  {
    std::cerr << "Usage: " << argv[0] << " <bindings path> <dtb> <output>" << std::endl;
    return -1;
  }

  try
  {
    /* Load binary FDT */

    std::ifstream dtb(argv[2], std::ios_base::in | std::ios_base::binary);

    if (!dtb)
    {
      std::cerr << "error: could not open " << argv[2] << std::endl;
      return -1;
    }

    dtgen::FDT fdt(dtb);

    /* Open output file */

    std::string bindings_path = argv[1];

    if (std::string(argv[2]) == "-")
    {
      dtgen::Generator(fdt, std::cout, bindings_path).generate();
    }
    else
    {
      std::ofstream out(argv[3], std::ios_base::out | std::ios_base::trunc);

      if (!out)
      {
        std::cerr << "error: could not open output file for "
                     "writing" << std::endl;
        return -1;
      }
      else
      {
        dtgen::Generator(fdt, out, bindings_path).generate();
      }
    }

    return 0;
  }
  catch (std::runtime_error& e)
  {
    std::cerr << "error: " << e.what() << std::endl;
    return -1;
  }
}
