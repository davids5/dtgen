DTC=dtc
CPP=cpp
DTCFLAGS=-@

-include $(DTS).d

DTS_PRE=$(DTS).pre

$(GENERATED_HEADER): $(DTB)
	$(DTGEN) $(BINDING_PATH) $(DTB) $(GENERATED_HEADER)

$(DTB): $(DTS)
	$(CPP) $(DTS_INCLUDE) -nostdinc -undef -x assembler-with-cpp $< -o $(DTS).pre
	$(DTC) $(DTCFLAGS) -I dts -O dtb $(DTS).pre --out $@
	rm -f $(DTS).pre

$(DTS).d: $(DTS)
	$(CPP) $(DTS_INCLUDE) -M -MT '$(DTB)' -nostdinc -undef -x assembler-with-cpp $< -o $@



