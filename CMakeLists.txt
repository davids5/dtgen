cmake_minimum_required(VERSION 3.5)

project(dtgen)

# Dependencies
find_package(Yaml-Cpp REQUIRED)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# DTB generation
#add_custom_target(dtb)
#add_custom_command(TARGET dtb
#                   COMMAND make
#                   WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/dts
#)

# Code Generation
#add_custom_target(generated_header)
#add_custom_command(TARGET generated_header
#                   COMMAND ${CMAKE_CURRENT_BINARY_DIR}/generator binding dts/final.dtb devicetree_gen.h
#                   DEPEND generator ${CMAKE_CURRENT_SOURCE_DIR}/dts/final.dtb
#                   WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
#)

# NuttX mockup initialization
#add_executable(mockup init_mockup.c)
#set_target_properties(mockup PROPERTIES LINKER_LANGUAGE C)
#add_dependencies(mockup dtb generated_header)

# Code Generator
add_compile_options(-fpermissive)
add_executable(dtgen src/main.cpp src/generator.cpp src/fdt.cpp
                     src/binding.cpp src/bindings.cpp)
target_link_libraries(dtgen fdt ${YAML_CPP_LIBRARIES})
