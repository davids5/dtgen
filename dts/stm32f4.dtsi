/* SPDX-License-Identifier: Apache-2.0 */

#include "armv7-m.dtsi"

/ {
	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-m4f";
			reg = <0>;
		};
	};

	sram0: memory@20000000 {
		compatible = "mmio-sram";
	};

	soc {
    family = "stm32";

		flash: flash-controller@40023c00 {
			compatible = "st,stm32-flash-controller", "st,stm32f4-flash-controller";
			label = "FLASH_CTRL";
			reg = <0x40023c00 0x400>;
			interrupts = <4 0>;

			#address-cells = <1>;
			#size-cells = <1>;

			flash0: flash@8000000 {
				compatible = "soc-nv-flash";
				label = "FLASH_STM32";

				write-block-size = <1>;
			};
		};

		rcc: rcc@40023800 {
			compatible = "st,stm32-rcc";
			#clock-cells = <2>;
			reg = <0x40023800 0x400>;
			label = "STM32_CLK_RCC";
		};

		pinctrl: pin-controller@40020000 {
			compatible = "st,stm32-pinmux";
			#address-cells = <1>;
			#size-cells = <1>;
			reg = <0x40020000 0x2000>;

			gpioa: gpio@40020000 {
				compatible = "st,stm32-gpio";
				gpio-controller;
				#gpio-cells = <2>;
				reg = <0x40020000 0x400>;
				label = "PORTA";
			};

			gpiob: gpio@40020400 {
				compatible = "st,stm32-gpio";
				gpio-controller;
				#gpio-cells = <2>;
				reg = <0x40020400 0x400>;
				label = "PORTB";
			};

			gpioc: gpio@40020800 {
				compatible = "st,stm32-gpio";
				gpio-controller;
				#gpio-cells = <2>;
				reg = <0x40020800 0x400>;
				label = "PORTC";
			};

			gpiod: gpio@40020c00 {
				compatible = "st,stm32-gpio";
				gpio-controller;
				#gpio-cells = <2>;
				reg = <0x40020c00 0x400>;
				label = "PORTD";
			};

			gpioe: gpio@40021000 {
				compatible = "st,stm32-gpio";
				gpio-controller;
				#gpio-cells = <2>;
				reg = <0x40021000 0x400>;
				label = "PORTE";
			};

			gpioh: gpio@40021c00 {
				compatible = "st,stm32-gpio";
				gpio-controller;
				#gpio-cells = <2>;
				reg = <0x40021c00 0x400>;
				label = "PORTH";
			};
		};

		iwdg: watchdog@40003000 {
			compatible = "st,stm32-watchdog";
			reg = <0x40003000 0x400>;
			label = "IWDG";
			status = "disabled";
		};

		wwdg: watchdog@40002c00 {
			compatible = "st,stm32-window-watchdog";
			reg = <0x40002C00 0x400>;
			label = "WWDG";
			interrupts = <0 7>;
			status = "disabled";
		};

		usart1: serial@40011000 {
			compatible = "st,stm32-usart", "st,stm32-uart";
			reg = <0x40011000 0x400>;
			interrupts = <37 0>;
			status = "disabled";
			label = "UART_1";
		};

		usart2: serial@40004400 {
			compatible = "st,stm32-usart", "st,stm32-uart";
			reg = <0x40004400 0x400>;
			interrupts = <38 0>;
			status = "disabled";
			label = "UART_2";
		};

		usart6: serial@40011400 {
			compatible = "st,stm32-usart", "st,stm32-uart";
			reg = <0x40011400 0x400>;
			interrupts = <71 0>;
			status = "disabled";
			label = "UART_6";
		};

		i2c1: i2c@40005400 {
			compatible = "st,stm32-i2c-v1";
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <0x40005400 0x400>;
			interrupts = <31 0>, <32 0>;
			interrupt-names = "event", "error";
			status = "disabled";
			label= "I2C_1";
		};

		i2c2: i2c@40005800 {
			compatible = "st,stm32-i2c-v1";
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <0x40005800 0x400>;
			interrupts = <33 0>, <34 0>;
			interrupt-names = "event", "error";
			status = "disabled";
			label= "I2C_2";
		};

		i2c3: i2c@40005c00 {
			compatible = "st,stm32-i2c-v1";
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <0x40005c00 0x400>;
			interrupts = <72 0>, <73 0>;
			interrupt-names = "event", "error";
			status = "disabled";
			label= "I2C_3";
		};

		spi1: spi@40013000 {
			compatible = "st,stm32-spi";
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <0x40013000 0x400>;
			interrupts = <35 5>;
			status = "disabled";
			label = "SPI_1";
		};

		i2s1: i2s@40013000 {
			compatible = "st,stm32-i2s";
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <0x40013000 0x400>;
			interrupts = <35 5>;
			dmas = <&dma2 3 3 0x400 0x3
				&dma2 2 3 0x400 0x3>;
			dma-names = "tx", "rx";
			status = "disabled";
			label = "I2S_1";
		};

		rtc: rtc@40002800 {
			compatible = "st,stm32-rtc";
			reg = <0x40002800 0x400>;
			interrupts = <41 0>;
			prescaler = <32768>;
			status = "disabled";
			label = "RTC_0";
		};

		adc1: adc@40012000 {
			compatible = "st,stm32-adc";
			reg = <0x40012000 0x400>;
			interrupts = <18 0>;
			status = "disabled";
			label = "ADC_1";
			#io-channel-cells = <1>;
		};

		dma1: dma@40026000 {
			compatible = "st,stm32-dma";
			#dma-cells = <4>;
			reg = <0x40026000 0x400>;
			interrupts = <11 0 12 0 13 0 14 0 15 0 16 0 17 0 47 0>;
			status = "disabled";
			label = "DMA_1";
		};

		dma2: dma@40026400 {
			compatible = "st,stm32-dma";
			#dma-cells = <4>;
			reg = <0x40026400 0x400>;
			interrupts = <56 0 57 0 58 0 59 0 60 0 68 0 69 0 70 0>;
			st,mem2mem;
			status = "disabled";
			label = "DMA_2";
		};
	};

	otgfs_phy: otgfs_phy {
		compatible = "usb-nop-xceiv";
		#phy-cells = <0>;
		label = "OTGFS_PHY";
	};
};

